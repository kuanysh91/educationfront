module.exports = {
  productionSourceMap: false,
  assetsDir: 'spa',
  devServer: {
    proxy: {
      "^/api": {
        target: "https://localhost:8080",
        ws: true,
        changeOrigin: true,
      },
    },
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
    },
    public: "http://localhost:8080",
    disableHostCheck: true,
  },
};

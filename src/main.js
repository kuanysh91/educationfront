import Vue from 'vue'
import App from './App.vue'
import router from './router';
import store from './store';
import Clipboard from 'v-clipboard'
import Bootstrap from 'bootstrap/dist/js/bootstrap';
import BootstrapVue from "bootstrap-vue"
// import BootstrapSelect from 'bootstrap-select/js/bootstrap-select'
import request from "./services/request";
import API from './services/api';
import VNotification from './components/VNotification';

Vue.config.productionTip = false;
Vue.use(BootstrapVue)
Vue.use(Clipboard);
Vue.use(Bootstrap)
// Vue.use(BootstrapSelect)
Vue.prototype.$http = request;
Vue.prototype.$getAPI = API;
Vue.use(VNotification);


new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

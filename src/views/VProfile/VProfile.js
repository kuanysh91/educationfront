import VProfileCard from "./components/VProfileCard";
import { profileList } from "./jsons";

export default {
  name: 'VProfile',

  components: {
    VProfileCard,
  },

  data() {
    return {
      isProfileAside: false,
      profileMenu: profileList,
    }
  },

  methods: {
    checkActiveMenu(item) {
      return this.$route.meta?.id.includes(item.id);
    }
  }
}
import IconBase from "@/components/icons/IconBase";
import IconPen from "@/components/icons/components/IconPen";
import IconPencil from "@/components/icons/components/IconPencil";
import IconCloudDownload from "@/components/icons/components/IconCloudDownload";
import IconHeart from "@/components/icons/components/IconHeart";
import IconEye from "@/components/icons/components/IconEye";
import IconSpin from "@/components/icons/components/IconSpin";

export default {
  name: 'VProfileArticle',

  components: {
    IconBase,
    IconPen,
    IconPencil,
    IconCloudDownload,
    IconHeart,
    IconEye,
    IconSpin
  },

  data() {
    return {
      articleList: [],
    }
  },

  async mounted() {
    await this.fetchData();
  },

  methods: {
    async fetchData(){
      try {
        const res = await this.$http({
          url: this.$getAPI('article.articles'),
          params: {
            me: true,
          },
        });
        this.articleList = res.rows;
      } catch (error) {
        console.log(error);
      }
    },
    handleClickCreate() {
      this.$router.push({name: 'VProfileArticleCreate'});
    },
    handleClickEdit(id) {
      this.$router.push({
        name: 'VProfileArticleEdit',
        params: { id }
      });
    },
    async handleClickLike(id, index) {
      this.articleList[index].isLiked = !this.articleList[index].isLiked;

      const res = await this.$http({
        url: this.$getAPI('article.like', {
          id
        }),
        method: 'POST'
      });
      if (!res.error) {
        this.$notify({
          type: 'success',
          title: 'Успешно',
          text: res?.message,
        });
        await this.fetchData();
      }
    }
  }
}
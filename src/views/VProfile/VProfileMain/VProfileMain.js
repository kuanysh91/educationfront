import VFavoriteCard from "../components/VFavoriteCard";
import IconBase from "@/components/icons/IconBase";
import IconHeart from "@/components/icons/components/IconHeart";
import IconPrice from "@/components/icons/components/IconPrice";
import IconLogoStroke from '@/components/icons/components/IconLogoStroke';

export default {
  name: 'VProfileMain',

  components: {
    VFavoriteCard,
    IconBase,
    IconHeart,
    IconPrice,
    IconLogoStroke
  },

  data() {
    return {
      courseList: [
        {
          title: 'Геометрия',
          section: '8-сынып'
        },
        {
          title: 'Геометрия',
          section: '8-сынып'
        },
        {
          title: 'Геометрия',
          section: '8-сынып'
        },
        {
          title: 'Геометрия',
          section: '8-сынып'
        }
      ],
      activityList: [
        {
          name: 'Математика',
          time: '3 ч 21 м',
          done: true
        },
        {
          name: 'Математика',
          time: '3 ч 21 м',
          done: false
        },
        {
          name: 'Математика',
          time: '3 ч 21 м',
          done: false
        },
        {
          name: 'Математика',
          time: '3 ч 21 м',
          done: false
        }
      ],
      scheduleList: [
        {
          name: 'Математика',
          timeStart: '09:00',
          timeEnd: '09:45'
        },
        {
          name: 'Математика',
          timeStart: '09:00',
          timeEnd: '09:45'
        },
        {
          name: 'Математика',
          timeStart: '09:00',
          timeEnd: '09:45'
        },
        {
          name: 'Математика',
          timeStart: '09:00',
          timeEnd: '09:45'
        },
        {
          name: 'Математика',
          timeStart: '09:00',
          timeEnd: '09:45'
        }
      ],
      rewardList: [
        {
          title: 'Сертификат по Физике',
          number: '1234123'
        },
        {
          title: 'Сертификат по Физике',
          number: '1234123'
        },
        {
          title: 'Сертификат по Физике',
          number: '1234123'
        },
        {
          title: 'Сертификат по Физике',
          number: '1234123'
        },
        {
          title: 'Сертификат по Физике',
          number: '1234123'
        }
      ],
      favoriteList: [
        {
          name: 'КОМБО Информатика 5-11 сынып',
          price: '3 500₸',
          bonus: '2000'
        },
        {
          name: 'КОМБО Информатика 5-11 сынып',
          price: '3 500₸',
          bonus: '2000'
        },
        {
          name: 'КОМБО Информатика 5-11 сынып',
          price: '3 500₸',
          bonus: '2000'
        },
        {
          name: 'КОМБО Информатика 5-11 сынып',
          price: '3 500₸',
          bonus: '2000'
        },
        {
          name: 'КОМБО Информатика 5-11 сынып',
          price: '3 500₸',
          bonus: '2000'
        },
        {
          name: 'КОМБО Информатика 5-11 сынып',
          price: '3 500₸',
          bonus: '2000'
        },
        {
          name: 'КОМБО Информатика 5-11 сынып',
          price: '3 500₸',
          bonus: '2000'
        }
      ],
      promoList: [
        {
          title: 'ЕНТ КОМБО основные предметы',
          rating: '4.5',
          bonus: 2000,
          price: '3 500₸'
        },
        {
          title: 'ЕНТ КОМБО основные предметы',
          rating: '4.5',
          bonus: 2000,
          price: '3 500₸'
        },
        {
          title: 'ЕНТ КОМБО основные предметы',
          rating: '4.5',
          bonus: 2000,
          price: '3 500₸'
        },
        {
          title: 'ЕНТ КОМБО основные предметы',
          rating: '4.5',
          bonus: 2000,
          price: '3 500₸'
        }
      ]
    }
  }
}
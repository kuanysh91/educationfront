import VInput from '../../components/VCustomInput';
import IconBase from "../../components/icons/IconBase";
import IconApple from "../../components/icons/components/IconApple";
import IconGoogle from "../../components/icons/components/IconGoogle";
import IconFacebook from "../../components/icons/components/IconFacebook";
import IconRestore from "../../components/icons/components/IconRestore";

export default {
  name: 'VAuth',

  components: {
    VInput,
    IconApple,
    IconGoogle,
    IconFacebook,
    IconBase,
    IconRestore
  },

  data() {
    return {
      fieldLogin: '',
      fieldPassword: ''
    }
  },

  methods: {
    async sendRequest() {
      const res = await this.$http({
        url: this.$getAPI('auth.login'),
        method: 'POST',
        data: {
          email: this.fieldLogin,
          password: this.fieldPassword,
        }
      });
      if (!res.error) {
        await this.$store.dispatch('auth', res?.access_token);
        await this.$router.push({name: 'VProfileMain'});
      }
    },
  }
}
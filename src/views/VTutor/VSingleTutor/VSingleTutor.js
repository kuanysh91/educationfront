import VInput from '../../../components/VCustomInput'
import VueSlickCarousel from 'vue-slick-carousel'
import 'vue-slick-carousel/dist/vue-slick-carousel.css'
// optional style for arrows & dots
import 'vue-slick-carousel/dist/vue-slick-carousel-theme.css'
import { KinesisContainer, KinesisElement } from 'vue-kinesis'

export default {
  name: 'VSingleTutor',

  data() {
    return {
        selectedCheckboxId: 0,
        tutorReviewOptions: {
            dots: true,
            infinite: true,
            speed: 700,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true,
                        arrows: false,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                    }
                },
            ]
        },
        teacherCardOptions: {
            dots: true,
            infinite: true,
            speed: 700,
            slidesToShow: 4,
            slidesToScroll: 2,
            autoplay: true,
            autoplaySpeed: 3000,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true,
                        arrows: false,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                    }
                },
            ]
        },
        parallaxSliderOptions: {
            responsive: [
                {
                    breakpoint: 2200,
                    settings: 'unslick'
                },
                {
                    breakpoint: 600,
                    settings: {
                        dots: false,
                        infinite: true,
                        speed: 1000,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 1000,
                        variableWidth: true,
                        arrows: false,
                    }
                }
            ]
        },
        form: {
            name: null,
            number: null,
            extraInfo: null,
        },
        eduCheckBoxData: [
            {
              text: 'Начальная школа',
              subText: '1-4 класс'
            },
            {
                text: 'Средняя школа',
                subText: '5-9 класс'
            },
            {
                text: 'Старшая школа',
                subText: '10-11 класс'
            },
            {
                text: 'Студент',
                subText: 'или взрослый'
            }
        ]
    }
  },

    components: {
        VInput,
        VueSlickCarousel,
        KinesisContainer,
        KinesisElement
    },
    methods: {
        handleSelectCheckbox(id) {
            this.selectedCheckboxId = id;
        }
    }
}

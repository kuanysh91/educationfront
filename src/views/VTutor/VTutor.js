import VTutorCard from "./components/VTutorCard";
import VSelect from "../../components/VCustomSelect";
import VInput from "../../components/VCustomInput";


export default {
  name: 'VTutor',

  data() {
    return {
        subjects: ['Алгебра', 'Английский', 'Искусствовведение'],
        cities: ['Алматы', 'Астана', 'Караганда'],
        places: ['Медеуский', 'Алмалинский', 'Жетысуйский'],
        selectedSubject: 'Алгебра',
        selectedCity: 'Алматы',
        selectedPlace: 'Медеуский',
        priceFrom: null,
        priceTo: null,
        filterOpened: false,
        tutorList: []
    }
  },
    async mounted() {
        await this.fetchData();
    },
    components: {
        VTutorCard,
        VSelect,
        VInput
    },
    methods: {
        toggleFilter() {
            this.filterOpened = !this.filterOpened
        },
        async fetchData(){
            try {
                const res = await this.$http({
                    url: this.$getAPI('tutors.tutors'),
                    // params: {
                    //     per_page: 20,
                    // },
                });
                this.tutorList = res.rows;
            } catch (error) {
                console.log(error);
            }
        },
    }
}

export default {
  isLoggedIn: (state) => !!state.token,
  profilePhoto: (state) => state.photo,
  getUserFullName: (state) => state.user?.name,
  getDarynSum: (state) => state.user?.daryn
}
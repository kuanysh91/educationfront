import router from "../../router";

export default {
  auth({ commit }, token) {
    commit('auth', token);
    window.localStorage.setItem('token', token);
  },
  logout({ commit }) {
    commit('logout');
    window.localStorage.removeItem('token');
    router.push('/auth/login');
  }
}
export default {
  auth(state, token) {
    state.token = token;
  },
  logout(state) {
    state.token = '';
    state.user = '';
  },
  setProfile(state, payload) {
    state.user = payload;
  }
}
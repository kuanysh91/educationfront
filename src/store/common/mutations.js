export default {
  setLocale(state, locale) {
    state.lang = locale;
    window.localStorage.setItem('language', locale);
    window.location.reload();
  },
  setOverlay(state, payload) {
    state.isOverlay = payload;
  }
}
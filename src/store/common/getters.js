export default {
  interfaceLang: (state) => state.lang,
  isOverlay: (state) => state.isOverlay
}
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import { quillEditor } from 'vue-quill-editor';
import Quill from 'quill';
import ImageUploader from 'quill-image-uploader';

const Clipboard = Quill.import('modules/clipboard');
const Delta = Quill.import('delta');

class PlainClipboard extends Clipboard {
  onPaste(e) {
    e.preventDefault();
    const range = this.quill.getSelection();
    const text = e.clipboardData.getData('text/plain');
    const delta = new Delta()
      .retain(range.index)
      .delete(range.length)
      .insert(text);
    const index = text.length + range.index;
    const length = 0;
    this.quill.updateContents(delta);
    this.quill.setSelection(index, length);
    this.quill.scrollIntoView();
  }
}

Quill.register('modules/clipboard', PlainClipboard, true);
Quill.register('modules/imageUploader', ImageUploader);

export default {
  components: {
    quillEditor,
  },

  data() {
    return {
      quillEditorOptions: {
        theme: 'snow',
        placeholder: '',
        modules: {
          toolbar: [
            [
              'clean',
              'bold',
              'italic',
            ],
            [
              {
                list: 'ordered',
              },
              {
                list: 'bullet',
              },
            ],
            ['image', 'video'],
          ],
          imageUploader: {
            upload: this.fileUpload,
          },
        },
      },
    };
  },

  methods: {
    fileUpload() {

    }
  }
}
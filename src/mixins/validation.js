export default {
  data() {
    return {
      validation: {},
    }
  },

  methods: {
    addValidationError(field, value) {
      if (!Array.isArray(this.validation[field])) {
        this.validation[field] = [];
      }

      this.validation = {
        ...this.validation,
        [field]: [...value],
      };
    },
  }
}
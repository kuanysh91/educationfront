import errorHandler from "../helpers/errorHandler";

export default [
  {
    path: 'olympiads',
    name: 'VOlympiads',
    component: () => import('../views/VOlympiads').catch(errorHandler),
  }
]

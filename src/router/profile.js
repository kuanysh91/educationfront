import errorHandler from "../helpers/errorHandler";
import render from '../helpers/renderRouterView';

export default [
  {
    path: 'profile',
    component: () => import('../views/VProfile').catch(errorHandler),
    meta: {
      authorization: true,
    },
    children: [
      {
        path: '',
        name: 'VProfileMain',
        component: () => import('../views/VProfile/VProfileMain').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'profile'
        },
      },
      {
        path: 'edit',
        name: 'VProfileEdit',
        component: () => import('../views/VProfile/VProfileEdit').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'profile'
        },
      },
      {
        path: 'article',
        component: {
          render,
        },
        children: [
          {
            path: '',
            name: 'VProfileArticle',
            component: () => import('../views/VProfile/VProfileArticle').catch(errorHandler),
            meta: {
              authorization: true,
              id: 'article'
            },
          },
          {
            path: ':id',
            name: 'VProfileArticleEdit',
            component: () => import('../views/VProfile/VProfileArticle/VProfileArticleForm')
              .catch(errorHandler),
            meta: {
              authorization: true,
              id: 'article'
            },
          },
          {
            path: 'create',
            name: 'VProfileArticleCreate',
            component: () => import('../views/VProfile/VProfileArticle/VProfileArticleForm').catch(errorHandler),
            meta: {
              authorization: true,
              id: 'article'
            },
          }
        ]
      },
      {
        path: 'favorite',
        name: 'VProfileFavorite',
        component: () => import('../views/VProfile/VProfileFavorite').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'favorite'
        },
      },
      {
        path: 'referral',
        name: 'VProfileReferral',
        component: () => import('../views/VProfile/VProfileReferral').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'referral'
        },
      },
      {
        path: 'transaction',
        name: 'VHistoryTransaction',
        component: () => import('../views/VProfile/VHistoryTransaction').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'transaction'
        },
      },
      {
        path: 'notification',
        name: 'VProfileNotice',
        component: () => import('../views/VProfile/VProfileNotice').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'notification'
        },
      },
      {
        path: 'history-call',
        component: {
          render,
        },
        children: [
          {
            path: '',
            name: 'VHistoryCall',
            component: () => import('../views/VProfile/VHistoryCall').catch(errorHandler),
            meta: {
              authorization: true,
              id: 'history-call'
            },
          },
          {
            path: 'details',
            name: 'VHistoryCallDetail',
            component: () => import('../views/VProfile/VHistoryCallDetail').catch(errorHandler),
            meta: {
              authorization: true,
              id: 'history-call'
            },
          }
        ]
      },
      {
        path: 'reward',
        name: 'VProfileReward',
        component: () => import('../views/VProfile/VProfileReward').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'reward'
        },
      },
      {
        path: 'wallet',
        name: 'VProfileWallet',
        component: () => import('../views/VProfile/VProfileWallet').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'wallet'
        },
      }
    ]
  }
];
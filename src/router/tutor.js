import errorHandler from "../helpers/errorHandler";

export default [
  {
    path: 'tutors',
    name: 'VTutor',
    component: () => import('../views/VTutor').catch(errorHandler),
  },
    {
        path: 'tutors/:id',
        name: 'VSingleTutor',
        component: () => import('../views/VTutor/VSingleTutor').catch(errorHandler),
        meta: {
            authorization: true,
        },
    },
]

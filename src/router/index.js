import Vue from 'vue';
import VueRouter from "vue-router";
import render from '@/helpers/renderRouterView';
import store from '../store';
import request from "../services/request";
import API from '../services/api';

import profile from "./profile";
import authenticate from "./authenticate";
import olympiads from "./olympiads";
import tutor from "./tutor";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: {
      render
    },
    children: [
      {
        path: '',
        redirect: '/profile'
      },
      ...authenticate,
      ...profile,
      ...olympiads,
      ...tutor
    ]
  }
];

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes
});

router.beforeEach(async (to, from, next) => {
  if (store.getters.isLoggedIn && !store.state.auth.user) {
    try {
      const res = await request({
        url: API('auth.profile'),
      });
      if (!res.error) {
        store.commit('setProfile', res);
      }
    } catch (err) {
      return store.dispatch('logout');
    }
  }

  if (to.meta.authorization && !store.getters.isLoggedIn) {
    return next('/auth/login');
  }

  return next();
});

export default router;

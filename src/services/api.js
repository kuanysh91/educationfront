import _ from 'lodash';

const uri = {
  auth: {
    login: '/api/auth/login',
    profile: '/api/user-info'
  },
  menus: {
    menu: '/api/menus',
  },
  article: {
    articles: '/api/articles',
    getArticleById: '/api/articles/:id',
    remove: '/api/articles/:id',
    like: '/api/articles/:id/like',
    edit: '/api/articles/:id',
    degreeDict: '/api/degrees',
    categoryDict: '/api/article/categories'
  },
    tutors: {
        tutors: '/api/tutor/list',
    }
}

export default (uriName = '', params = {}) => {
  let uriStr = _.get(uri, uriName) || '';

  if (uriStr !== '') {
    if (_.isObject(params)) {
      _.forEach(params, (value, key) => {
        uriStr = uriStr.replace(`:${key}`, value);
      });
    }
  } else {
    console.warn('getUri: is empty uriName: ', uriName);
  }

  return uriStr;
};

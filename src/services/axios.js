import axios from 'axios';
import store from '../store';

export const instance = axios.create({
  baseURL: process.env.NODE_ENV === 'production' ? '' : process.env.VUE_APP_SERVER_URL,
});

instance.defaults.headers.common = {
  Accept: 'application/json',
  'X-Localization': store.state.common.lang,
  'Content-Type': 'application/json',
};

instance.interceptors.request.use(request => {
  if (store.state.auth.token) {
    request.headers['Authorization'] = `Bearer ${store.state.auth.token}`;
  }
  return request;
}, (error) => {
  return Promise.reject(error);
});

instance.interceptors.response.use(async(res) => {
  return (res && res.data) || res;
},  async (error) => {
  if (error.response.status === 401 && store.state.auth.token !== '') {
    await store.dispatch('logout');
  }

  return Promise.reject(error);
});
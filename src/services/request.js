import { instance } from './axios';
import Vue from 'vue';

export const errorCodes = {
  ERR_401: 401,
  ERR_404: 404,
  ERR_422: 422,
  ERR_423: 423,
  ERR_500: 500,
};

function notify(type, message, title, duration) {
  const params = {
    type,
    title: title,
    text: message ,
  };

  if (duration) {
    params.duration = duration;
  }

  Vue.prototype.$notify(params);
}

function catchDefault(code, data) {
  const response = {
    error: true,
    errorType: code,
    data: {
      message: data.message ?? data,
    },
  };

  if (!data.action) {
    notify('error', response.data.message);
  }

  return response;
}

function catch422(data) {
  const response = {
    error: true,
    errorType: errorCodes.ERR_422,
    data,
  };
  if (!data.errors) {
    notify('error', data.message);
  } else if (Array.isArray(data.errors)) {
    notify('error', data.errors[0]);
  } else if (this && data.errors) {
    Object.keys(data.errors).forEach((key) => {
      notify('error', data.errors[key], '', 5000);
      // this.addValidationError(key, data.errors[key]);
    });
  }

  return response;
}

export default async function request(options = {}) {
  try {
    const res = await instance(options);
    return (res && res.data) || res;
  } catch (err) {
    if (err && err.response) {
      const {
        status,
        data,
      } = err.response;

      switch(status) {
        case errorCodes.ERR_422: return catch422.call(this, data);
        default: return catchDefault.call(this, status, data);
      }
    }

    return err;
  }
}

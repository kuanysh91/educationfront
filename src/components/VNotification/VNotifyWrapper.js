import { v4 as uuidv4 } from 'uuid';

export default {
  name: 'VNotifyWrapper',

  data() {
    return {
      items: [],
      maxVisible: 3,
    };
  },

  mounted() {
    this.$eventHub.$on('v-notify-show', this.addNotify);
  },

  methods: {
    addNotify(item) {
      if (this.items.length >= this.maxVisible) {
        this.removeNotify(0)();
      }

      const key = uuidv4();

      if (item.duration) {
        setTimeout(() => {
          this.removeNotifyByKey(key);
        }, item.duration);
      }

      this.items.push({
        ...item,
        key
      });
    },

    removeNotify(index) {
      return () => {
        this.items.splice(index, 1);
      }
    },

    removeNotifyByKey(key) {
      if (key) {
        this.items = this.items.filter((item) => key !== item.key);
      }
    },

    elNotify(item, itemIdx) {
      return this.$createElement('div', {
        key: item.text + item.key,
        staticClass: 'v-notify',
        class: {
          'v-notify--success': item.type === 'success',
          'v-notify--danger': item.type === 'error',
          'v-notify--has-author': item.type === 'author',
          // 'v-notify--warning': item.type === 'warning',
          // 'v-notify--info': item.type === 'info',
        },
      }, [
        this.$createElement('div', {
          staticClass: 'v-notify__img',
        }, [
          this.$createElement('i', { staticClass: 'icon' }),
          this.$createElement('img', {
            attrs: { src: '/img/main/avatar.png' }
          })
        ]),
        this.$createElement('div', {
          staticClass: 'v-notify__content',
        }, [
          this.$createElement('div', { staticClass: 'v-notify__title' }, item.title),
          this.$createElement('div', {
            staticClass: 'v-notify__text',
            domProps: {
              innerHTML: item.text,
            }
          })
        ]),
        this.$createElement('button', {
          staticClass: 'btn-plain v-notify__close',
          domProps: {
            innerHTML: '<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
              '<path d="M6 6L12 12" stroke="#76787A" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
              '<path d="M12 6L6 12" stroke="#76787A" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
              '</svg>'
          },
          on: {
            click: this.removeNotify(itemIdx),
          },
        })
      ])
    },
  },
  render(h) {
    return h('transition-group', {
      staticClass: 'v-notify__wrapper v-notify__wrapper--pos-top-right',
    }, this.items.map((item, itemIdx) => {
      if (item.position === 'top-right') {
        return this.elNotify(item, itemIdx);
      }

      return undefined;
    }));
  },
}